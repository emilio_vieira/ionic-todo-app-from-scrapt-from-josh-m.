import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { AddItemPage } from '../add-item/add-item';
import { ItemDetailPage } from '../item-detail/item-detail';
import { DataProvider } from '../../providers/data/data';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public items: Array<any> = [];

  /**
   * Remember before how we assigned a type of any to the homePage variable? 
   * Now we are assigning a type of NavController 
   * to the navCtrl parameter in the constructor. 
   * 
   * This is how dependency injection works in Ionic, 
   * and is basically a way of telling the application 
   * “we want navCtrl to be a reference to NavController“. 
   * 
   * By adding the public keyword in front of it, 
   * it automatically creates a member variable for us. 
   * This means that we can now reference the NavController anywhere 
   * in this class by using this.navCtrl.
   * 
   * @param navCtrl 
   */
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public dataService: DataProvider
  ) {
    
    this.dataService.getData().then( (todos)=>{

      this.items = todos;

    });
  }

  /**
   * we use the ionViewDidLoad lifecycle hook to do this 
   * which is triggered as soon as the page is loaded
   */
  ionViewDidLoad(){
    
    // this.items = [
      // {title: 'hi1', description:'test1'},
      // {title: 'hi2', description:'test2'},
      // {title: 'hi3', description:'test3'},
    // ]

  }

  addItem(){

    let addModal = this.modalCtrl.create(AddItemPage);

    addModal.onDidDismiss( (item)=>{
      if(item){
        this.saveItem(item);
      }
    });

    addModal.present();
  }

  saveItem(item){

    if(this.items === null)
      this.items = [];

    
    // Push into array items
    this.items.push(item);

    // Push onto Storage
    this.dataService.save(this.items);
    
  }

  viewItem(item){
    this.navCtrl.push(ItemDetailPage, {
      item: item
    })
  }

}