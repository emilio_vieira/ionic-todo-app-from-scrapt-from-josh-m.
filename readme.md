## Stickys Notes - Build a Todo App from Scratch with Ionic. 

##### Tutorial by Josh Morony Blog. 
##### https://www.joshmorony.com/build-a-todo-app-from-scratch-with-ionic-2-video-tutorial/

 **IMPORTANT - About "Lazy Loading"**
 * When pages are generated in Ionic something called “lazy loading” is set up by default. 
  To keep things simple, we will not be covering that in this tutorial.
  * To make sure you do not run into any issues  you should delete the add-item.module.ts file that is generated.  
  * The @IonicPage() decorator should also be removed from the add-item.ts file.
  * Whenever we create a new page, we need to ensure that it is imported into our app.module.ts
  and declared in the declarations and entryComponents arrays. 
 
 **About Members Class**
   * Remember before how we assigned a type of any to the homePage variable?
  Now we are assigning a type of NavController to the navCtrl parameter in the constructor.
   * This is how dependency injection works in Ionic, and is basically a way of telling the application “we want navCtrl to be a reference to NavController“.
   * By adding the public keyword in front of it, it automatically creates a member variable for us.
   * This means that we can now reference the NavController anywhere in this class by using this.navCtrl.