import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  /**
   * We are able to declare variables above the constructor 
   * to make them member variables or class members.
   * 
   * they will be accessible throughout the entire class by referencing this.myVar 
   * and it will also be available to your templates. 
   * In this case, we are creating a class member called rootPage 
   * that we will be able to access throughout this class, and in the template.
   */

  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

